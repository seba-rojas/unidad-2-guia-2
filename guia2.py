#se importa la libreria gi y pandas
import gi 
import pandas
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
#se abre el archivo en formato panda
def open_file():

    try:
        with open("drug200.csv", "r") as archivo:
            data = pandas.read_csv(archivo)
            print(data)
    except IOError:
        data = []
    return data


    
    
# def save_file(data):
#     with open("estudiantes.json", "w") as archivo:
#     json.dump(data, archivo, indent=4)

#se crea la clase de la ventana principal
class Principal():
    def __init__(self):
        #se traen los archivos necesarios como el archivo glade
        builder = Gtk.Builder()
        builder.add_from_file("guia_2.ui")
        ventana = builder.get_object("principal")
        ventana.set_default_size(800, 600)
        #se le da un titulo a la ventana
        ventana.set_title("--Trabajo guía 2-- ")
        ventana.connect("destroy", Gtk.main_quit)

        data = open_file()
#de agregan los botones con sus respectiva funcion
        boton_agregar = builder.get_object("añadir_btn")
        boton_agregar.connect("clicked", self.openventana1)

        boton_eliminar = builder.get_object("eliminar_btn")
        #boton_eliminar.connect("clicked", self.delete_select_data)
 
        boton_resumen = builder.get_object("resumen_btn")
        #boton_resumen.connect("clicked", self.openventana3)

        self.tree = builder.get_object("treedrugs")
        
    #se crean el largo de las columnas para el treeview
        largo_columna = len(data.columns)
        self.modelo = Gtk.ListStore(*(largo_columna*[str]))
        self.tree.set_model(model= self.modelo)
        cell = Gtk.CellRendererText()
        
        ventana.show_all()

#se ve el largo de columnas y 
        for item in range(len(data.columns)):
            column = Gtk.TreeViewColumn(data.columns[item], cell, text = item)
            self.tree.append_column(column)
        self.load_data_from_csv()
#uns funcion que carge todo el archivo 
    def load_data_from_csv(self):
        data = open_file()
        for item in data.values:
            line = [str(x) for x in item]
            self.modelo.append(line)
#una funcion de borre todo el archivo
    def delete_all_data(self):
        for index in range(len(self.modelo)):
            iter_ = self.modelo.get_iter(0)
            self.modelo.remove(iter_)

#una funcion que cumple la funcion del primer boton el cual es habrir la ventana secundaria
    def openventana1(self, btn=None):
        dialogo = ventana_trabajo()
        print("que pasa")
        

    # def delete_select_data(self, btn=None):
    #     model, it = self.tree.get_selection().get_selected()
    #     if model is None or it is None:
    #         return
        
    #     data = open_file()
    #     for item in data:
    #         if(item[""] == model.get_value(it, 0)):
    #             data.remove(item)
    #     save_file(data)
        
    #     self.delete_all_data()
    #     self.load_data_from_csv()
        
#se crea la clase para la segunda ventana 
class ventana_trabajo():
    def __init__(self):
#se llaman los archivos necesarios
        builder = Gtk.Builder()
        builder.add_from_file("guia_2.ui")
        ventana_2 = builder.get_object("ventana_trabajo")

#se agregan os botones 
        boton_aceptar = builder.get_object("aceptar_btn")
    
        boton_cancelar = builder.get_object("cancelar_btn")


        self.entry_edad = builder.get_object("edad")
#se agregan los bomboboxtext necesarios ccada uno con sus respectivas listas
#comboboxtext del sexo
        lista_sexo = ["Seleccione una opcion", "F", "M"]
        self.comboboxtext_sexo = builder.get_object("combotext_sexo")
        for item in lista_sexo:
            self.comboboxtext_sexo.append_text(item)
            self.comboboxtext_sexo.set_active(0)
        self.comboboxtext_sexo.connect("changed", self.comboboxtext_sexo_change)    

#comboboxtext de la presion
        lista_presion = ["Seleccione una opcion", "HIGH", "LOW", "NORMAL"]
        self.comboboxtext_presion = builder.get_object("combotext_presion")
        for item in lista_presion:
            self.comboboxtext_presion.append_text(item)
            self.comboboxtext_presion.set_active(0)
        self.comboboxtext_presion.connect("changed", self.comboboxtext_presion_change)

        self.entry_proporcion = builder.get_object("proporcion_Na_K")

#comboboxtext del nivel de colesterol
        lista_colesterol = ["Seleccione una opcion", "HIGH", "NORMAL"]
        self.comboboxtext_colesterol = builder.get_object("combotext_colesterol")
        for item in lista_colesterol:
            self.comboboxtext_colesterol.append_text(item)
            self.comboboxtext_colesterol.set_active(0)
        self.comboboxtext_colesterol.connect("changed", self.comboboxtext_colesterol_change)

        
#comboboxtext del tipo de droga
        lista_droga = ["Seleccione una opcion", "DrugA", "DrugB", "DrugC", "DrugX", "DrugY"]
        self.comboboxtext_droga = builder.get_object("combotext_droga")
        for item in lista_droga:
            self.comboboxtext_droga.append_text(item)
            self.comboboxtext_droga.set_active(0)
        self.comboboxtext_droga.connect("changed", self.comboboxtext_droga_change)
        
        

        ventana_2.show_all()
#funcion que retiene el valor de los comboboxtext
    def comboboxtext_sexo_change(self, cmb=None):
        valor1 = self.comboboxtext_sexo.get_active_text()
        if self.comboboxtext_sexo.get_active() != 0:
            print(valor1)
    
    def comboboxtext_presion_change(self, cmb=None):
        valor2 = self.comboboxtext_presion.get_active_text()
        if self.comboboxtext_presion.get_active() != 0:
            print(valor2)
    
    def comboboxtext_colesterol_change(self, cmb=None):
        valor3 = self.comboboxtext_colesterol.get_active_text()
        if self.comboboxtext_colesterol.get_active() != 0:
            print(valor3)

    def comboboxtext_droga_change(self, cmb=None):
        valor4 = self.comboboxtext_droga.get_active_text()
        if self.comboboxtext_droga.get_active() != 0:
            print(valor4)


if __name__ == "__main__":
    Principal()
    Gtk.main()